<?php

namespace Avanti\BlockWelcomeEmail\Plugin\Magento\Customer\Api;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Api\Data\CustomerExtensionFactory;

class CustomerRepositoryPlugin
{
    const FIELD_NAME = 'send_email';

    protected $extensionFactory;

    public function __construct(CustomerExtensionFactory $extensionFactory)
    {
        $this->extensionFactory = $extensionFactory;
    }

    public function afterGet(CustomerRepositoryInterface $customerRepository, CustomerInterface $customer)
    {
        $sendEmail = $customer->getCustomAttribute('send_email');
        $extensionAttributes = $customer->getExtensionAttributes();
        $extensionAttributes = $extensionAttributes ? $extensionAttributes : $this->extensionFactory->create();
        $extensionAttributes->setSendEmail($sendEmail);
        $customer->setExtensionAttributes($extensionAttributes);

        return $customer;
    }

    public function afterGetExtensionAttributes(
        CustomerInterface $entity,
        CustomerExtensionInterface $extension = null
    ) {
        if ($extension === null) {
            $extension = $this->extensionFactory->create();
        }

        return $extension;
    }
}